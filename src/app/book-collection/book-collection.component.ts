import { CartItem } from './../state/collection.reducer';
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-book-collection',
  templateUrl: './book-collection.component.html'
})
export class BookCollectionComponent {
  @Input() books?: ReadonlyArray<CartItem> | null;
  @Output() remove = new EventEmitter();
}
