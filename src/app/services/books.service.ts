import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class GoogleBooksService {

  constructor(private http: HttpClient) { }

  getBooks(): Observable<any> {
    const url = 'https://www.googleapis.com/books/v1/volumes?maxResults=5&orderBy=relevance&q=oliver%20sacks';
    return this.http.get(url);
  }

}
