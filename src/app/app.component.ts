import { addBook, removeBook, fetchBooks } from './state/books.actions';
import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as fromBooks from './state/books.selectors';
import * as fromCollection from './state/collection.selectors';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  books$ = this.store.pipe(select(fromBooks.selectAll));
  booksLoading$ = this.store.pipe(select(fromBooks.selectLoading));
  bookCollection$ = this.store.pipe(select(fromCollection.selectCart));
  itemsCount$ = this.store.pipe(select(fromBooks.selectCount));
  cartCount$ = this.store.pipe(select(fromCollection.selectCount));

  onAdd(bookId: any) {
    this.store.dispatch(addBook({ bookId }));
  }

  onRemove(bookId: any) {
    this.store.dispatch(removeBook({ bookId }));
  }

  constructor(
    private store: Store
  ) { }

  ngOnInit() {
    this.store.dispatch(fetchBooks())
  }
}
