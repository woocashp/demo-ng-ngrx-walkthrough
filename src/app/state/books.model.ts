export interface Book {
    id: string;
    volumeInfo: {
      title: string;
      authors: Array<string>;
    };
}
export interface BooksModel<T> {
  books: T[],
  totalItems: number
}
