import { EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from "@ngrx/store";
import { addBook, removeBook } from "./books.actions";

export interface CartItem {
  id: string,
  count: number,
  title?: string
};

export const cartAdapter: EntityAdapter<CartItem> = createEntityAdapter<CartItem>();

export const state = cartAdapter.getInitialState({
  loaded: false,
  loading: true
});

export const collectionReducer = createReducer(
  state,
  on(removeBook, (state, { bookId: book }) => {
    const found = state.entities[book.id];
    return found?.count === 1
      ? cartAdapter.removeOne(book.id, state)
      : cartAdapter.upsertOne({ ...book, count: book.count - 1 }, state)
  }),
  on(addBook, (state, { bookId }) => {
    const found = state.entities[bookId];
    return cartAdapter.upsertOne({ id: bookId, ...{ count: found?.count ? found.count + 1 : 1 } }, state);
  })
);
