import { Book } from './books.model';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from "@ngrx/store";
import { retrievedBookList, fetchBooks } from "./books.actions";

export interface BooksState extends EntityState<Book> {
  totalItems: number,
  loading: boolean
}

export const booksAdapter: EntityAdapter<Book> = createEntityAdapter<Book>();

const init: BooksState = booksAdapter.getInitialState({
  totalItems: 0,
  loading: false
})

export const booksReducer = createReducer(
  init,
  on(fetchBooks, (state) => ({ ...state, loading: true })),
  on(retrievedBookList, (state, { books, totalItems }) => {
    return booksAdapter.addAll(books, { ...state, totalItems, loading: false });
  })
);
