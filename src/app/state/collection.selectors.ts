import { EntityState } from '@ngrx/entity';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CartItem, cartAdapter } from './collection.reducer';
import * as fromBooks from './books.selectors';

export const selectCartState = createFeatureSelector<EntityState<CartItem>>('collection');

export const { selectAll } = cartAdapter.getSelectors(selectCartState);

export const selectCart = createSelector(
    selectAll,
    fromBooks.selectEntities,
    (cart, books) => cart.map((item) => ({ ...item, title: books[item.id]?.volumeInfo.title }))
)

export const selectCount = createSelector(
    selectAll,
    (state) => state.reduce((acc, item) => acc + item.count, 0)
)
