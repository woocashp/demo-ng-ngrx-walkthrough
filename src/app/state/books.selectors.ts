import { createFeatureSelector, createSelector } from "@ngrx/store";
import * as fromBooks from './books.reducer';

export const selectBooksState = createFeatureSelector<fromBooks.BooksState>("books");

export const { selectAll, selectEntities } = fromBooks.booksAdapter.getSelectors(selectBooksState);

export const selectCount = createSelector(
  selectBooksState,
  (state) => state.totalItems
)

export const selectLoading = createSelector(
  selectBooksState,
  (state) => state.loading
)
