import { GoogleBooksService } from '../services/books.service';
import { fetchBooks, retrievedBookList } from './books.actions';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, Effect } from '@ngrx/effects';
import { exhaustMap, map } from 'rxjs/operators';

@Injectable()
export class BooksEffects {

  @Effect()
  fetch$ = createEffect(() => this.actions$.pipe(
    ofType(fetchBooks),
    exhaustMap(_ => this.booksService.getBooks()),
    map(({ items: Book, totalItems }) => retrievedBookList({ books: Book, totalItems }))
  ))

  constructor(
    private actions$: Actions,
    private booksService: GoogleBooksService
  ) { }

}
